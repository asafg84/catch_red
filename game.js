(function(global) {
    var self = this,
        _score = 0,
        time = 2000,
        timer,
        level = 0;
    
    function unset_winner(board) {
        board.querySelector('.winner').classList.remove('winner');
    }
    
    function set_winner(board) {
        var squares = board.querySelectorAll('div'),
            num_of_squares = squares.length,
            winner = Math.floor(Math.random() * (num_of_squares - 1));
        squares[winner].classList.add('winner');
    }
    
    function times_out() {
        _score -= 5;
        level -= 5;
        if (level % 20 == 0)
            check_level();
        update_score();
        next_turn();
    }
    
    function on_click() {
        clearTimeout(timer);
        var targetElement = event.target || event.srcElement;
        if (targetElement.classList.contains('winner')) {
            _score += 5;
            level += 5;
            if (level % 20 == 0)
                check_level();
            update_score();
            next_turn(time);
        }
        else {
            _score -= 5;
            level -= 5;
            if (level % 20 == 0)
                check_level();
            update_score();
            next_turn(time);
        }
    }
    
    function update_score() {
        score.innerHTML = 'Score: ' + _score;
    }
    
    function check_level() {
        if (level >= 20) {
            level = 0;
            time -= 250;
            console.log('up level!!');
        }
        else if (level <= 0) {
            level = 0;
            time += 250;
            console.log('down level!!');
        }
    }
    
    function next_turn() {
        unset_winner(board);
        set_winner(board);
        timer = setTimeout(times_out, time);
    }
    
    global.catch_red.start = function(board, score) {
        set_winner(board);
        var squares = board.querySelectorAll('div');
        for (var i = 0, len = squares.length; i < len; i++) {
            squares[i].addEventListener('click', on_click);
        }
        timer = setTimeout(times_out, time);
    }
}(this));